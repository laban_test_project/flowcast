var assert = require('assert');
const fs = require('fs');
const os = require('os');
const path = require('path');
const cliParser = require("../bin/index")

describe('Print one word to the console', function () {
    it("works", function() {
        assert.doesNotThrow(() =>  cliParser.print("print"));
    }) 
});

describe('List everything in a folder given the folder path', function() {
        it("works", function () {
          let tmpDir;
          try {
            tmpDir = fs.mkdtempSync(path.join(os.tmpdir(), "flowcast"));
            fs.writeFileSync(path.join(tmpDir, "test1"), "");
            fs.writeFileSync(path.join(tmpDir, "test2"), "");
            assert.equal(
              2,
              cliParser.listFiles(tmpDir),
              "did not find all files"
            );
          } finally {
            fs.rmSync(tmpDir, { recursive: true });
          }
        });
    
})



  