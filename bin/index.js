#!/usr/bin/env node
const fs = require('fs');

function parseThirdArgument(value) {
    console.log(value);
}

function listFiles(directory) {
    return fs.readdirSync(directory).length
}

module.exports.print = parseThirdArgument
module.exports.listFiles = listFiles